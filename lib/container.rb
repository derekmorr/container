class Container
  def self.from_value(value)
    if (value.nil? || value.empty?)
      Empty.new
    else
      Full.new(value)
    end
  end
end

class Empty < Container
  def initialize
  end

  def empty?
    true
  end

  def length
    0
  end

  def value
    []
  end

  def add_if_empty(item)
    Container.from_value(item)
  end
end

class Full < Container
  attr_reader :element

  def initialize(element)
    @element = element
  end

  def empty?
    false
  end

  def length
    1
  end

  def value
    [@element].flatten
  end

  def add_if_empty(item)
    self
  end

end
