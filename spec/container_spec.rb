# encoding: utf-8

require "spec_helper"
require 'container'

RSpec.describe 'the Container monad' do

  describe "from_value" do
    it "produces an Empty from nil" do
      container = Container.from_value(nil)
      expect(container.empty?).to be true
    end

    it "produces an Empty from empty input" do
      container = Container.from_value([])
      expect(container.empty?).to be true
    end

    it "produces a Full from non-nil, non-empty input" do
      container = Container.from_value([1,2,3])
      expect(container.empty?).to be false
    end
  end

  describe "value" do
    it "produces [] when empty" do
      container = Container.from_value([])
      value = container.value
      expect(value).to eql([])
    end

    it "produces the value in an array when full" do
      container = Container.from_value("howdy")
      value = container.value
      expect(value).to eq(["howdy"])
    end

    it "does not return a nested array when full" do
      container = Container.from_value([1,2,3])
      value = container.value
      expect(value).to eql([1,2,3])
    end
  end

  describe "length" do
    it "return 0 when empty" do
      container = Container.from_value([])
      expect(container.length).to eql(0)
    end

    it "returns 1 when full" do
      container = Container.from_value([1,2,3])
      expect(container.length).to eql(1)
    end
  end

  describe "add_if_empty" do
    it "should not call the block for full containers" do
      container = Container.from_value([1,2,3])
      new_container = container.add_if_empty("foobar")
      expect(new_container.value).to eql([1,2,3])
    end

    it "should yield empty if passed an empty" do
      container = Container.from_value([])
      new_container = container.add_if_empty([])
      expect(new_container.empty?).to be true
    end

    it "should yield empty if passed nil" do
      container = Container.from_value([])
      new_container = container.add_if_empty(nil)
      expect(new_container.empty?).to be true
    end

    it "should yield full if an empty is passed a non-nil, non-empty value" do
      container = Container.from_value([])
      new_container = container.add_if_empty("howdy")
      expect(new_container.value).to eql(["howdy"])
    end
  end
end
